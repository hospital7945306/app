import axios from "axios";

// Клиент для доступа к api
const client = axios.create({
    baseURL: 'http://192.168.1.33:3000/api/v1/',
    timeout: 1000
});

export default client