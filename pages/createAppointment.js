import * as react from "react"

import {Pressable, StyleSheet, Text, View, TextInput, FlatList} from "react-native";

import globalStyles from './../styles/global'
import {useEffect, useState} from "react";
import DatePicker from "expo-datepicker";
import asyncStorage from "@react-native-async-storage/async-storage/src/AsyncStorage";
import client from "../axios/axios";
import {Dropdown} from "react-native-element-dropdown";

// создать новую запись к врачу
const CreateAppointmentScreen = ({ route, navigation }) => {
    //переменные токена
    const [token, setToken] = useState('')
    const [user, setUser] = useState({})

    useEffect(() => {
        const getToken = async () => {
            const raw = await asyncStorage.getItem('token')

            if (raw)
            {
                setToken(raw);
            }
        }

        getToken()
    }, []);

    useEffect(() => {
        const getUser = async () => {
            const raw = await asyncStorage.getItem('user')

            if (raw)
            {
                setUser(JSON.parse(raw));
            }
        }

        getUser()
    }, []);

    // переменные для категорий и врачей
    const [categories, setCategories] = useState([])
    const [doctors, setDoctors] = useState([])

    // получить категорию
    useEffect(() => {
        const get_appointments = () => {
            client.get('/categories', { params: {
                    access_token: token}

            })
                .then(
                    (data)=> setCategories(data.data)
                ).catch(err => console.log(err))
        }

        get_appointments()
    }, [token]);


    // переменные для записи
    const [category_id, setCategory] = new useState('')
    const [doctor_id, setDoctor] = new useState('')
    const [comment, setComment] = new useState('Боль в горле')
    const [date, setDate] = useState(new Date().toString());
    const [time, setTime] = useState('10:00');

    // получить врачей
    useEffect(() => {
        const get_appointments = () => {
            client.get('/users', { params: { "q[category_id_eq]": category_id,
                    access_token: token}

            })
                .then(
                    (data)=> setDoctors(data.data)
                ).catch(err => console.log(err))
        }

        get_appointments()
    }, [category_id]);

    // разметка
    return (
        <View style={globalStyles.container}>
            <View style={styles.titleBox}>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>Записаться на прием</Text>
            </View>
            <View style={styles.infoBox}>
                <View style={{alignItems: 'center'}}>
                    <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10 }} >Информация</Text>
                </View>
                <View style={styles.inputBox}>
                    <Text style={styles.inputTitle}>Категория</Text>
                    <Dropdown
                        data={categories}
                        labelField="name"
                        placeholder={'выберите категорию'}
                        onChange={(value) => setCategory(value.id)}
                        valueField="id"/>
                </View>

                <View style={styles.inputBox}>
                    <Text style={styles.inputTitle}>Врач</Text>
                    <Dropdown
                        data={doctors}
                        labelField="full_name"
                        disable={doctors.length === 0}
                        placeholder={'выберите врача'}
                        onChange={(value) => setDoctor(value.id)}
                        valueField="id"/>
                </View>
                <View style={styles.inputBox}>
                    <Text style={styles.inputTitle}>Дата и время</Text>
                    <DatePicker
                        date={date}
                        onChange={(date) => setDate(date)}
                    />
                    <FlatList data={[
                        '09:00',
                        '10:00',
                        '11:00',
                        '12:00',
                        '13:00',
                        '14:00',
                        '15:00',
                        '16:00',
                        '17:00',
                        '18:00',
                    ]} horizontal={true}
                              renderItem={
                        ({item})=> (
                            <Pressable onPress={() => setTime(item)}
                                style={{backgroundColor: '#33c', margin: 1, padding: 10, borderRadius: 15}}>
                                <Text style={{color: '#fff'}}>{item}</Text>
                            </Pressable>
                        )
                    }/>
                </View>
            </View>
            <View style={styles.commentBox}>
                <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10 }}>Комментарий</Text>
                <View style={styles.comment}>
                    <TextInput style={styles.input} onChangeText={setComment} value={comment}></TextInput>
                </View>
            </View>
            <View style={styles.bottomBox}>
                <Pressable style={globalStyles.button} onPress={()=> {
                    // создать запись к врачу
                    client.post('/appointments', {
                        doctor_id: doctor_id,
                        comment: comment,
                        recording_time: new Date(new Date(date).toDateString() + ' ' + time + ':00'),
                        access_token: token
                    }).then(()=> {
                        navigation.navigate('PatientMain');
                    }).catch(err => console.log(err))
                }}>
                    <Text style={globalStyles.buttonText} >Создать запись</Text>
                </Pressable>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    titleBox: {
        alignItems: 'center',
        padding: 20
    },
    infoBox: {
        padding: 10,
        justifyContent: 'flex-start',
        backgroundColor: '#fff',
        margin: 10,
        borderRadius: 10
    },
    commentBox: {
        padding: 10,
        justifyContent: 'flex-start',
        backgroundColor: '#fff',
        margin: 10,
        borderRadius: 10
    },
    comment: {
        padding: 5,
        justifyContent: 'flex-start',
        margin: 10,
        borderRadius: 10,
        backgroundColor: '#fafafa'
    },
    bottomBox: {
        flex: 1,
        justifyContent: 'flex-end',
        padding: 20
    },
    inputTitle:{
        color: '#c7c7c7'
    },
    input: {
        height: 20,
        borderBottomWidth: 1,
        borderColor: '#c7c7c7'
    },
    inputBox: {
        backgroundColor: '#fafafa',
        borderRadius: 15,
        padding: 15,
        margin: 5
    },
})

export default CreateAppointmentScreen