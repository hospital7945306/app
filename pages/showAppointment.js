import * as react from "react"

import {Pressable, StyleSheet, Text, View} from "react-native";

import globalStyles from './../styles/global'
import AsyncStorage from "@react-native-async-storage/async-storage";
import client from "../axios/axios";
import {useEffect, useState} from "react";
import asyncStorage from "@react-native-async-storage/async-storage/src/AsyncStorage";


const ShowAppointmentScreen = ({ route, navigation }) => {
    // переменные токена
    const [token, setToken] = useState('')
    const [user, setUser] = useState({})

    // получить токен
    useEffect(() => {
        const getToken = async () => {
            const raw = await asyncStorage.getItem('token')

            if (raw)
            {
                setToken(raw);
            }
        }

        getToken()
    }, []);

    // получить пользователя
    useEffect(() => {
        const getUser = async () => {
            const raw = await asyncStorage.getItem('user')

            if (raw)
            {
                setUser(JSON.parse(raw));
            }
        }

        getUser()
    }, []);

    // отрисовать просмотр записи к врачу
    return (
        <View style={globalStyles.container}>
            <View style={styles.titleBox}>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>Просмотр записи</Text>
            </View>
            <View style={styles.infoBox}>
                <View styleц={{alignItems: 'center'}}>
                    <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10 }} >Информация</Text>
                </View>
                <View style={styles.inputBox}>
                    <Text style={styles.inputTitle}>Дата и время</Text>
                    <Text style={styles.input}
                    >{new Date(route.params.data.recording_time).toLocaleString()}</Text>
                </View>

                <View style={styles.inputBox}>
                    <Text style={styles.inputTitle}>Категория</Text>
                    <Text style={styles.input}
                    >{route.params.data.doctor.category}</Text>
                </View>

                <View style={styles.inputBox}>
                    <Text style={styles.inputTitle}>Пациент</Text>
                    <Text style={styles.input}
                    >{route.params.data.patient.full_name}</Text>
                </View>
                <View style={styles.inputBox}>
                    <Text style={styles.inputTitle}>Доктор</Text>
                    <Text style={styles.input}
                    >{route.params.data.doctor.full_name}</Text>
                </View>
            </View>
            <View style={styles.commentBox}>
                <Text style={{ fontSize: 14, fontWeight: 'bold', margin: 10 }}>Комментарий</Text>
                <View style={styles.comment}>
                    <Text>{route.params.data.comment}</Text>
                </View>
            </View>
            <View style={styles.bottomBox}>
                <Pressable style={globalStyles.button} onPress={() => {
                    client.delete('/appointments/' + route.params.data.id,
                        {params: { access_token: token }})
                        .then(() => {
                            navigation.goBack();
                        })
                        .catch(err => console.log(err))
                }} >
                    <Text style={globalStyles.buttonText}  >Отменить запись</Text>
                </Pressable>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    titleBox: {
        alignItems: 'center',
        padding: 20
    },
    infoBox: {
        padding: 10,
        justifyContent: 'flex-start',
        backgroundColor: '#fff',
        margin: 10,
        borderRadius: 10
    },
    commentBox: {
        padding: 10,
        justifyContent: 'flex-start',
        backgroundColor: '#fff',
        margin: 10,
        borderRadius: 10
    },
    comment: {
        padding: 5,
        justifyContent: 'flex-start',
        margin: 10,
        borderRadius: 10,
        backgroundColor: '#fafafa'
    },
    bottomBox: {
        flex: 1,
        justifyContent: 'flex-end',
        padding: 20
    },
    inputTitle:{
        color: '#c7c7c7'
    },
    input: {
        height: 20,
        borderBottomWidth: 1,
        borderColor: '#c7c7c7'
    },
    inputBox: {
        backgroundColor: '#fafafa',
        borderRadius: 15,
        padding: 15,
        margin: 5
    },
})

export default ShowAppointmentScreen