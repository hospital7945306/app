import {FlatList, StyleSheet, Text, View, Image, Pressable} from "react-native";
import globalStyles from "../styles/global";
import asyncStorage from "@react-native-async-storage/async-storage/src/AsyncStorage";
import client from "../axios/axios";
import {useEffect, useState} from "react";

const DoctorMainScreen = ({ navigation }) => {
    // переменные для токена
    const [token, setToken] = useState('')
    const [user, setUser] = useState({})

    // плейсхолдер данных
    const data =[
        {
            "id":1,"patient":{"full_name":"Иванов Иван Иванович"},
            "doctor":{"full_name":"Петров Петр Петрович", "category":'Лор'},
            "comment": "Проблемы со слухом, боль и треск в левом ухе",
            "recording_time":"2024-10-10T12:30:00.000+03:00"},
        {
            "id":2,
            "patient":{"full_name":"Петров Денис Федорович"},
            "doctor":{"full_name":"Иванов Петр Петрович", "category": 'Терапевт'},
            "comment": "Кашель, насморк и боль в горле",
            "recording_time":"2024-10-10T14:00:00.000+03:00"}
    ]

    // записи
    const [appointment, setAppointment] = useState(data)

    // получить токен
    useEffect(() => {
        const getToken = async () => {
            const raw = await asyncStorage.getItem('token')

            if (raw)
            {
                setToken(raw);
            }
        }

        getToken()
    }, []);


    // получить пользователя
    useEffect(() => {
        const getUser = async () => {
            const raw = await asyncStorage.getItem('user')

            if (raw)
            {
                setUser(JSON.parse(raw));
            }
        }

        getUser()
    }, []);


    // получить записи к врачу
    useEffect(() => {
        const get_appointments = () => {
            client.get('/appointments', { params: { "q[user_id_eq]": user.id,
                    access_token: token}

            })
                .then(
                    (data)=> setAppointment(data.data)
                ).catch(err => console.log(err))
        }

        get_appointments()
    }, [token, user]);

    // комопнент вывода записи к врачу
    const Item = ({data}) => {
        return(
            <Pressable style={styles.item} onPress={() => {navigation.navigate('ShowAppointment', { data: data })} }>
                <View style={{ flex: 0.7, flexDirection: 'row' }}>
                    <View style={{flex: 1.4,flexDirection: 'row'}}>
                        <Image  style={{height: 15, width: 15}} source={{
                            uri: 'https://cdn.onlinewebfonts.com/svg/img_269620.png',
                        }}></Image>
                        <Text>Запись создана</Text>
                    </View>

                    <View style={{ flex: 1}}>
                        <Text>{ new Date(data.recording_time).toLocaleString() }</Text>
                    </View>
                </View>
                <Text>{['Пациент: ', data.patient.full_name] }</Text>
            </Pressable>
        )
    }

    // отрисовка компонента записи к врачу
    return (
        <View style={globalStyles.container}>
            <View style={styles.titleBox}>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>Мои записи</Text>
            </View>
            <View style={styles.dataBox}>
                <FlatList
                    data={appointment}
                    renderItem={({item}) => <Item data={item} />}
                    keyExtractor={item => item.id}
                />
            </View>
        </View>
    )
}

export default DoctorMainScreen

const styles = StyleSheet.create({
    titleBox: {
        alignItems: 'center',
        padding: 20
    },
    dataBox: {
        flex: 1,
        justifyContent: 'flex-start'
    },
    item: {
        backgroundColor: '#fff',
        margin: 10,
        padding: 10,
        borderRadius: 10,
        height: 90
    },
    itemTitle: {

    }
})