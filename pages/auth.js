import {Pressable, StyleSheet, Text, TextInput, View} from "react-native";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {useEffect, useState} from "react";
import globalStyles from "../styles/global";
import client from "./../axios/axios";
import JWT from "expo-jwt";

// страница авторизации
const  AuthScreen = ({navigation}) => {
    const [login, setLogin] = new useState('')
    const [password, setPassword] = new useState('')

    // переход на главное меню, если пользователь уже авторизован
    useEffect(() => {
        const getUser = () => {
            AsyncStorage.getItem('user').then((user)=> {
                if (JSON.parse(user).role === 'doctor') {
                    navigation.navigate('DoctorMain')
                } else {
                    navigation.navigate('PatientMain')
                }
            });
        }

        getUser()
    }, []);

    // разметка
    return (
        <View style={globalStyles.container}>
            <View style={styles.titleContainer}>
                <Text style={{fontSize: 20, fontWeight: 'bold'}}>Авторизация</Text>
            </View>
            <View style={styles.textInputsContainer}>
                <View style={globalStyles.inputBox}>
                    <Text style={globalStyles.inputTitle}>Email</Text>
                    <TextInput style={globalStyles.input} onChangeText={setLogin} value={login}></TextInput>
                </View>
                <View style={globalStyles.inputBox}>
                    <Text style={globalStyles.inputTitle}>Пароль</Text>
                    <TextInput style={globalStyles.input} onChangeText={setPassword} value={password}></TextInput>
                </View>
            </View>
            <View style={styles.bottomContainer}>
                <Pressable style={globalStyles.button} onPress={()=> {
                    // авторизация через api
                    client.post('/auth', {email: login, password: password})
                        .then((data) =>{

                            AsyncStorage.setItem('token', data.data.token)
                                .then(() => {
                                    AsyncStorage.setItem('user', JSON.stringify(data.data.user))
                                    console.log(data.data.user.role)
                                    if(data.data.user.role === 'doctor') {
                                        navigation.navigate('DoctorMain');
                                    } else {
                                        navigation.navigate('PatientMain');
                                    }
                                })
                        }).catch((error)=> console.log(error.message))
                }}>
                    <Text style={globalStyles.buttonText} >Войти</Text>
                </Pressable>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    titleContainer: {
        flex: 2,
        alignItems: 'center',
        height: '100%',
        justifyContent: 'flex-end',
        marginBottom: 30
    },
    textInputsContainer: {
        flex: 2,
        padding:20,
        alignItems: 'stretch',
        justifyContent: 'flex-start',
    },
    bottomContainer: {
        flex: 1
    }
});

export default AuthScreen