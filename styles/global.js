import {StyleSheet} from "react-native";

const globalStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff0',
    },
    inputTitle:{
        color: '#c7c7c7'
    },
    input: {
        height: 20,
        borderBottomWidth: 1,
        borderColor: '#c7c7c7'
    },
    inputBox: {
        backgroundColor: '#fff',
        borderRadius: 15,
        padding: 15,
        margin: 5
    },
    button: {
        backgroundColor: '#33c',
        color: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        margin: 5,
        height: 50
    },
    buttonText: {color: '#fff', fontSize: 16}
});

export default globalStyles;