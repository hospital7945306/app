import * as react from "react"


import { RootSiblingParent } from 'react-native-root-siblings';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import AuthScreen from "./pages/auth";
import PatientMainScreen from "./pages/patientMain";
import DoctorMainScreen from "./pages/doctorMain";
import ShowAppointmentScreen from "./pages/showAppointment";
import CreateAppointmentScreen from "./pages/createAppointment";
import {Button} from "react-native";

const Stack = createNativeStackNavigator();

// Навигация
function App() {
    return (
        <RootSiblingParent>
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen name="Auth" component={AuthScreen} options={{headerShown: false, title: 'Авторизация'}} />
                    <Stack.Screen name="PatientMain" component={PatientMainScreen} options={({ navigation, route }) =>({
                        title: 'Главное меню пациента',
                        headerRight: () => (<Button title={"+"} onPress={() => navigation.navigate('CreateAppointment')}></Button>)
                    })} />
                    <Stack.Screen name="DoctorMain" component={DoctorMainScreen} options={{title: 'Главное меню врача'}} />
                    <Stack.Screen name="ShowAppointment" component={ShowAppointmentScreen} options={{title: 'Просмотр'}} />
                    <Stack.Screen name="CreateAppointment" component={CreateAppointmentScreen} options={{title: 'Создать'}} />
                </Stack.Navigator>
            </NavigationContainer>
        </RootSiblingParent>
    );
}

export default App;
